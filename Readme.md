This setup is for i3-gaps and some other software

dependency:

-i3-gaps(PPA from Speed-Ricer)

-compton(PPA from Speed-Ricer)

-polybar(PPA from Speed-Ricer)

-vim(from official PPA)

-kitty(from official PPA)

-rofi(from official PPA)

-nitrogen(from official PPA)

-qutebrowser(from official PPA)

-vifm(from official PPA)

-mpd(from official PPA)

-ncmpcpp(from official PPA)

-xclip

-scrot

-feh

-policykit-dekstop-privileges

-policykit-1-gnome


Set-up to install those files

1.transfer the config of i3 to .config/i3/

2.Transfer the polybar folder to .config/

--Fix the monitor in poly config

3.Transfer the .fonts to ~/ 

4.Transfer the kitty config to .config/kitty

5.change the path in i3 config for wallpaper.sh

6.copy the .ncmpcpp folder to ~/

7.Install polybar theme gruvbox theme

8.copy the compton.conf file 
